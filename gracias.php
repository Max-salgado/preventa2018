
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Preventa 2018</title>
	
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="assets/css/home.min.css">
	<link rel="stylesheet" href="assets/css/validation.css" type="text/css">
	<link rel="stylesheet" href="assets/css/reset-encuesta.min.css?v2" type="text/css">

	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/library_validation.js"></script>
	<script type="text/javascript" src="assets/js/main-encuesta.js"></script>
    <script type="text/javascript" src="assets/js/validation.js"></script>

</head>
<body>
<!--WRAPPER PRINCIPAL-->
<div class="wrapper-principal">


	<div class="wrapper-header">
		<div class="wrapper-content content-header clearfix">
			
			<div class="logo-head">
				<img src="http://www.crp.pe/preventa2018/assets/images/logo-casa-radio.jpg" alt="">
			</div>

			<div class="titular-header">
				<div class="subtitle">
					Preventa 2018
				</div>
			</div>
			
		</div>
	</div> <!-- fin wrapper-header -->





	<div class="wrapper-contenido">
		<div class="wrapper-content">
			<div class="contenido-home">
				<!-- <div class="contenido-izq tabla-cell">
				 	<div class="lema-preventa">
						5 conexiones que debes experimentar
					</div>
				</div> -->
				


				<div class="title-add">
					<p>interactiva</p>
				</div>
				


				<div class="encuesta-add">
			
					
					<p class="enunciado-add gracias">Gracias por participar</p>
					

					<a href="http://www.crp.pe/preventa2018-interactiva/"><button type="submit" class="btn-crp" value="Submit" style="height: 56px;">VOLVER A INICIO</button></a>
					

				</div> <!-- fin encuesta-add -->







				<!-- <div class="contenido-dere tabla-cell">

					<div class="lema-preventa text-center">
						5 conexiones que debes experimentar
					</div>

					<div class="logo-preventa">
						<div class="tapa_img">
							<img src="http://www.crp.pe/preventa2018/assets/images/tapa-ruleta.png" alt="">
						</div>
						<div class="base_img">
							<img src="http://www.crp.pe/preventa2018/assets/images/base-ruleta.png" alt="">
						</div>
					</div>

				</div> -->



			</div> <!-- fin contenido-home -->

		</div> <!-- fin wrapper-content -->

	</div> <!-- fin wrapper-contenido -->
 

	<!--FOOTER-->
		<div class="wrapper-footer">
		<div class="wrapper-content">
			<div class="footer clearfix">
				<div class="logo-crp">
					<img src="assets/images/logo-crp-2017.jpg" alt="">
				</div>
				<div class="grupo-crp">
					<ul>
						<li>
							<a target="_blank" href="https://ritmoromantica.pe/">Ritmo Romántica</a>
						</li>
						<li>
							<a target="_blank" href="http://radionuevaq.pe/">Nueva Q</a>
						</li>
						<li>
							<a target="_blank" href="https://moda.com.pe/">Moda</a>
						</li>
						<li>
							<a target="_blank" href="https://lainolvidable.pe/">La Inolvidable</a>
						</li>
						<li>
							<a target="_blank" href="https://oasis.pe/">Oasis</a>
						</li>
						<li>
							<a target="_blank" href="http://radiomar.pe/">Radiomar</a>
						</li>
						<li>
							<a target="_blank" href="https://planeta.pe/">Planeta</a>
						</li>
						<li>
							<a target="_blank" href="http://www.radioinca.com.pe/">Inca</a>
						</li>
						<li>
							<a target="_blank" href="http://radiomagica.pe/">Mágica</a>
						</li>
					</ul>
					<span>Conectamos</span>
				</div>
			</div>
		</div>
	</div>	<!--FIN FOOTER-->
</div>	
<!--FIN WRAPPER PINCIPAL-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script>
function aMays(e, elemento) {
tecla=(document.all) ? e.keyCode : e.which; 
 elemento.value = elemento.value.toUpperCase();
}
var angle= [0, 67, 140, 216, 285, 360];
var pos = 0;
function AnimateRotate(){
    var elem = $(".tapa_img");
    $({deg: angle[pos]}).animate({deg: angle[pos+1]}, {
        duration: 2000,
        step: function(now){
            elem.css({
                 transform: "rotate(" + now + "deg)"
            });
        }
    });
    pos++
    if(pos == angle.length){
    	pos = 0
    }
    window.setTimeout(function() { AnimateRotate() }, 5000)
}
AnimateRotate()
</script>

</body>
</html>