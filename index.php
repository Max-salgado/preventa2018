
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Preventa 2018</title>
	
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="assets/css/home.min.css">
	<link rel="stylesheet" href="assets/css/validation.css" type="text/css">
	<link rel="stylesheet" href="assets/css/reset-encuesta.min.css?v1" type="text/css">

	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/main-encuesta.js"></script>

</head>
<body>
<!--WRAPPER PRINCIPAL-->
<div class="wrapper-principal">


	<div class="wrapper-header">
		<div class="wrapper-content content-header clearfix">
			
			<div class="logo-head">
				<img src="http://www.crp.pe/preventa2018/assets/images/logo-casa-radio.jpg" alt="">
			</div>

			<div class="titular-header">
				<div class="subtitle">
					Preventa 2018
				</div>
			</div>
			
		</div>
	</div> <!-- fin wrapper-header -->





	<div class="wrapper-contenido">
		<div class="wrapper-content">
			<div class="contenido-home">
				<!-- <div class="contenido-izq tabla-cell">
				 	<div class="lema-preventa">
						5 conexiones que debes experimentar
					</div>
				</div> -->
				


				<div class="title-add">
					<p>interactiva</p>
				</div>
				


				<div class="encuesta-add">
					<div class="add-num"><p>1</p></div>

					<p class="enunciado-add">Este es un  cantante de musica pop….<br>Es español, pero tiene fuertes vínculos con la música latina…<br>Empezó su carrera musical en 1993…<br>Canta en español y en ingles…<br>Es hijo de dos personalidades del arte y el entretenimiento….<br>Se apellida Presley<br>Su papa se llama Julio Iglesias…</p>

					<div class="op-add">
						<form action="pregunta-2.php" method="post">
							<div class="wrapp-op">	
								

								<div class="op-gen">
									<input type="radio" name="respuesta" class="chekSelect" value="2">
									<p class="op-texto">Bon Jovi</p>
								</div>

								<div class="op-gen">
									<input type="radio" name="respuesta" class="chekSelect" value="3">
									<p class="op-texto">Jerry Rivera</p>
								</div>

								<div class="op-gen">
									<input type="radio" name="respuesta" class="chekSelect" value="1">								
									<p class="op-texto">Enrique Iglesias</p>
								</div>

								<div class="op-gen">
									<input type="radio" name="respuesta" class="chekSelect" value="4">
									<p class="op-texto">Luis Miguel</p>
								</div>
						
							</div> <!-- fin wrapp-op -->

						</form>

					</div> <!-- fin op-add -->

				</div> <!-- fin encuesta-add -->







				<!-- <div class="contenido-dere tabla-cell">

					<div class="lema-preventa text-center">
						5 conexiones que debes experimentar
					</div>

					<div class="logo-preventa">
						<div class="tapa_img">
							<img src="http://www.crp.pe/preventa2018/assets/images/tapa-ruleta.png" alt="">
						</div>
						<div class="base_img">
							<img src="http://www.crp.pe/preventa2018/assets/images/base-ruleta.png" alt="">
						</div>
					</div>

				</div> -->



			</div> <!-- fin contenido-home -->

		</div> <!-- fin wrapper-content -->

	</div> <!-- fin wrapper-contenido -->
 

	<!--FOOTER-->
		<div class="wrapper-footer">
		<div class="wrapper-content">
			<div class="footer clearfix">
				<div class="logo-crp">
					<img src="assets/images/logo-crp-2017.jpg" alt="">
				</div>
				<div class="grupo-crp">
					<ul>
						<li>
							<a target="_blank" href="https://ritmoromantica.pe/">Ritmo Romántica</a>
						</li>
						<li>
							<a target="_blank" href="http://radionuevaq.pe/">Nueva Q</a>
						</li>
						<li>
							<a target="_blank" href="https://moda.com.pe/">Moda</a>
						</li>
						<li>
							<a target="_blank" href="https://lainolvidable.pe/">La Inolvidable</a>
						</li>
						<li>
							<a target="_blank" href="https://oasis.pe/">Oasis</a>
						</li>
						<li>
							<a target="_blank" href="http://radiomar.pe/">Radiomar</a>
						</li>
						<li>
							<a target="_blank" href="https://planeta.pe/">Planeta</a>
						</li>
						<li>
							<a target="_blank" href="http://www.radioinca.com.pe/">Inca</a>
						</li>
						<li>
							<a target="_blank" href="http://radiomagica.pe/">Mágica</a>
						</li>
					</ul>
					<span>Conectamos</span>
				</div>
			</div>
		</div>
	</div>	<!--FIN FOOTER-->
</div>	
<!--FIN WRAPPER PINCIPAL-->
<script>  
 $(document).ready(function() {   
   $('input[name=respuesta]').click(function(){  

   		var respuesta = this.value;
   		if(respuesta == 1)
        {
            $('form').submit();  
        }
        
   });  
  });  
</script>  

</body>
</html>