
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Preventa 2018</title>
	
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="assets/css/home.min.css">
	<link rel="stylesheet" href="assets/css/validation.css" type="text/css">
	<link rel="stylesheet" href="assets/css/reset-encuesta.min.css?v1" type="text/css">

	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/library_validation.js"></script>
	<script type="text/javascript" src="assets/js/main-encuesta.js"></script>
    <script type="text/javascript" src="assets/js/validation.js"></script>

</head>
<body>
<!--WRAPPER PRINCIPAL-->
<div class="wrapper-principal">


	<div class="wrapper-header">
		<div class="wrapper-content content-header clearfix">
			
			<div class="logo-head">
				<img src="http://www.crp.pe/preventa2018/assets/images/logo-casa-radio.jpg" alt="">
			</div>

			<div class="titular-header">
				<div class="subtitle">
					Preventa 2018
				</div>
			</div>
			
		</div>
	</div> <!-- fin wrapper-header -->





	<div class="wrapper-contenido">
		<div class="wrapper-content">
			<div class="contenido-home">
				<!-- <div class="contenido-izq tabla-cell">
				 	<div class="lema-preventa">
						5 conexiones que debes experimentar
					</div>
				</div> -->
				


				<div class="title-add">
					<p>interactiva</p>
				</div>
				

			<form action="gracias.php" method="post" onSubmit="">
				<div class="encuesta-add encuesta-add-inn">
					<div class="add-num"><p>3</p></div>
					
					<div class="enunciado-add">
						<div class="wrapp-op">
							<div class="line">
								<p>Súbeme la</p>
								<select class="select">
									<option value="0"></option>
									<option value="1">RADIO</option>
								</select>
							</div>


							<div class="line">
								<p>Que esta es mi</p>
								<select class="select">
									<option value="0"></option>
									<option value="1">CANCIÓN</option>
								</select>
							</div>


							<div class="line">
								<p>Siente el</p>
								<select class="select">
									<option value="0"></option>
									<option value="1">BAJO</option>
								</select>
								<p class="reset-line-mob">que va subiendo</p>
							</div>

							<div class="line">
								<p>Tráeme el</p>
								<select class="select">
									<option></option>
									<option value="1">ALCOHOL</option>
								</select>
							</div>


							<div class="line">
								<p>que quita el</p>
								<select class="select">
									<option value="0"></option>
									<option value="1">DOLOR</option>
								</select>
							</div>


							<div class="line">
								<p>Vamos a juntar la</p>
								<select class="select" id="select-final" name="select-final">
									<option value="0"></option>
									<option value="1">ARENA</option>
								</select>
								<p class="reset-line-mob">y el sol.</p>
							</div>
						</div>
						
					</div>

				</div> <!-- fin encuesta-add -->

			</form>





				<!-- <div class="contenido-dere tabla-cell">

					<div class="lema-preventa text-center">
						5 conexiones que debes experimentar
					</div>

					<div class="logo-preventa">
						<div class="tapa_img">
							<img src="http://www.crp.pe/preventa2018/assets/images/tapa-ruleta.png" alt="">
						</div>
						<div class="base_img">
							<img src="http://www.crp.pe/preventa2018/assets/images/base-ruleta.png" alt="">
						</div>
					</div>

				</div> -->



			</div> <!-- fin contenido-home -->

		</div> <!-- fin wrapper-content -->

	</div> <!-- fin wrapper-contenido -->
 

	<!--FOOTER-->
		<div class="wrapper-footer">
		<div class="wrapper-content">
			<div class="footer clearfix">
				<div class="logo-crp">
					<img src="assets/images/logo-crp-2017.jpg" alt="">
				</div>
				<div class="grupo-crp">
					<ul>
						<li>
							<a target="_blank" href="https://ritmoromantica.pe/">Ritmo Romántica</a>
						</li>
						<li>
							<a target="_blank" href="http://radionuevaq.pe/">Nueva Q</a>
						</li>
						<li>
							<a target="_blank" href="https://moda.com.pe/">Moda</a>
						</li>
						<li>
							<a target="_blank" href="https://lainolvidable.pe/">La Inolvidable</a>
						</li>
						<li>
							<a target="_blank" href="https://oasis.pe/">Oasis</a>
						</li>
						<li>
							<a target="_blank" href="http://radiomar.pe/">Radiomar</a>
						</li>
						<li>
							<a target="_blank" href="https://planeta.pe/">Planeta</a>
						</li>
						<li>
							<a target="_blank" href="http://www.radioinca.com.pe/">Inca</a>
						</li>
						<li>
							<a target="_blank" href="http://radiomagica.pe/">Mágica</a>
						</li>
					</ul>
					<span>Conectamos</span>
				</div>
			</div>
		</div>
	</div>	<!--FIN FOOTER-->
</div>	
<!--FIN WRAPPER PINCIPAL-->

<script>  
 $(document).ready(function() {   
   $("select").change(function(){  

   		var select = this.value;

	    if (select == "1") {

	        if( $( this ).attr( 'name' ) ==  "select-final")
	        {
	        	$('form').submit();  
	        }
	    }
        
   });  
  });  
</script>  

</body>
</html>